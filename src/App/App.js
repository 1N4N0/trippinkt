import React, { Component } from 'react';
import {SectionsContainer, Section, Header, Footer} from 'react-fullpage';
import fire from '../fire';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { messages: [], email: '', password: '', maxWidth: '90%', maxHeight: '450', toggleElement: '-' }; // <- set up react state
  }
  componentWillMount(){
    /* Create reference to messages in Firebase Database */
    let messagesRef = fire.database().ref('messages').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
      let message = { text: snapshot.val(), id: snapshot.key };
      this.setState({ messages: [message].concat(this.state.messages) });
    })
  }

  // <div>
  //   <ul>
  //     {
  //       this.state.messages.map( message => <li key={message.id}>{message.text}</li> )
  //     }
  //   </ul>
  // </div>

  getGalleryHeaders = () => {
    return(
      <div className="galleryHeaderScroller">
        <div className="galleryHeaderListWrpr cf">
          <span className="galleryListHeader active">Fantasy</span>
          <span className="galleryListHeader">Geometric</span>
          <span className="galleryListHeader">Graphic</span>
          <span className="galleryListHeader">Life</span>
          <span className="galleryListHeader">Minimalist</span>
          <span className="galleryListHeader">Portrait</span>
        </div>
      </div>
      )
  }

  getGalleryImages = () => {
    console.log("should return images")
  }

  toggleMapHeight = () => {
    if(this.state.maxHeight === '450'){
      this.setState({maxHeight : '150', toggleElement: '+'})
    }else{
      this.setState({maxHeight: '450', toggleElement: '-'})
    }
  }


  render() {
    let options = {
      sectionClassName:     'section',
      anchors:              ['sectionOne', 'sectionTwo', 'sectionThree', 'sectionFour'],
      scrollBar:            false,
      navigation:           true,
      verticalAlign:        false,
      arrowNavigation:      true
    };
    return (
      <div>
        <div className="App-header cf">
          <ul className="appLogo"><a href="#">Trippink Tattoos</a></ul>
          <ul className="menuWrpr">
            <a href="#sectionOne">Home</a>
            <a href="#sectionTwo">Gallery</a>
            <a href="#sectionThree">About</a>
            <a href="#sectionFour">Contact</a>
          </ul>
        </div>
        <Footer>
          <a href="">Copyright Trippink Tattoos 2017</a>
          <a href="">Handcrafted with Love</a>
        </Footer>
        <SectionsContainer className="container" {...options}>
          <Section className="custom-section firstSection" color="#ffffff">
            <div className="trippinkLogo"></div>
            <div className="mapsHeader cf">
              <div className="mapsName">Virtual Studio Tour</div><div className="mapsMinimize" onClick={()=>this.toggleMapHeight()}>{this.state.toggleElement}</div>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2sin!4v1496748454938!6m8!1m7!1sF%3A-QSqdzAe9U1g%2FWRWj69pCKyI%2FAAAAAAAAK1Y%2FjGKuEoO3HgETrJ43vnm684opJ9yD5mb_QCLIB!2m2!1d12.91012199279364!2d77.59001936763525!3f124!4f0!5f0.7820865974627469" width={this.state.maxWidth} height={this.state.maxHeight} ></iframe>
          </Section>
          <Section className="custom-section" color="#ffffff">
            <div className="galleryHeaderWrpr">
            <div className="galleryHeader">Our Work</div>
            {this.getGalleryHeaders()}
            </div>
            <div className="galleryImageContainer">
            Gallery Images
            {this.getGalleryImages()}
            </div>
            <div className="galleryTestimonialHeader">What people say</div>
            <div className="galleryTestimonial">Testimonials</div>
          </Section>
          <Section className="custom-section" color="#222222">
            <div className="aboutHeading">About Heading</div>
            <div className="aboutDesc">
              <div className="aboutHistory">History</div>
              <div className="aboutTeam">Team</div>
              <div className="aboutIntership">Internship</div>
              <div className="aboutEquipmentWrpr">Equipment</div>
              <div className="aboutContact">Contact</div>
            </div>
          </Section>
          <Section className="custom-section" color="#ffffff">
          <div className="mapsWrapper"></div>
          <div className="addressWrpr"></div>
          <div className="formWrapper"></div>
          </Section>
        </SectionsContainer>
      </div>
    );
  }
}

export default App;