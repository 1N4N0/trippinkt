import React, { Component } from 'react';
import fire from '../fire';

import './Profile.css';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = { messages: [], email: '', password: '', loggedIn: false }; // <- set up react state
    }
    componentWillMount(){
    /* Create reference to messages in Firebase Database */
        let messagesRef = fire.database().ref('gallery').orderByKey().limitToLast(100);
        messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
            let message = { text: snapshot.val(), id: snapshot.key };
            this.setState({ messages: [message].concat(this.state.messages) });
        })

        this.checkAuth();
    }
    addMessage(e){
        e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
        var user = fire.auth().currentUser;
    // if(typeof user !== undefined){
        fire.database().ref('gallery').push( this.inputEl.value );
        this.inputEl.value = ''; // <- clear the input
    // }
    }

    authenticateUser = () => {
        console.log("will Authenticate");
        fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).catch(function(error) {
      // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
      // ...
        });
    }

    handleIdChange(e) {
        this.setState({ email: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    checkAuth = () => {
        fire.auth().signOut().then(function() {
            console.log("signed out")
        }, function(error) {
            console.log("signing out failed")
        });
        fire.auth().onAuthStateChanged(function(user) {
            if (user) {
                this.setState({loggedIn : true});
            console.log("User is signed in.");
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            var user = fire.auth().currentUser;

      // user.sendEmailVerification().then(function() {
      //   console.log("email sent");
      // }, function(error) {
      //   // An error happened.
      // });
      // ...
            } else {
                console.log("User is signed out.");
      // ...
            }
    });
}


// <form onSubmit={this.addMessage.bind(this)}>
//   <input type="text" ref={ el => this.inputEl = el }/>
//   <input type="submit"/>
//   <ul>
//     {
//       this.state.messages.map( message => <li key={message.id}>{message.text}</li> )
//     }
//   </ul>
// </form>

  // <div>
    // <input type="text" value={this.state.email} onChange={ this.handleIdChange.bind(this) } />
    // <input type="text" value={this.state.password} onChange={ this.handlePasswordChange.bind(this) }/>
    // <div onClick={()=>this.authenticateUser()}>Submit</div>
  // </div>

    getProfileComponent = () => {
        let loggedIn = this.state.loggedIn
        if(loggedIn){
            return(
                <form onSubmit={this.addMessage.bind(this)}>
                    <input type="text" ref={ el => this.inputEl = el }/>
                    <input type="submit"/>
                    <ul>
                        {
                            this.state.messages.map( message => <li key={message.id}>{message.text}</li> )
                        }
                    </ul>
                </form>
            )
        }else{
            return(
                <div>
                    <div className="loginContainer">
                        <div className="loginLabel">Login id</div>
                        <input type="text" className="loginInput" value={this.state.email} onChange={ this.handleIdChange.bind(this) } />
                        <div className="loginLabel">Password</div>
                        <input type="text" className="loginInput" value={this.state.password} onChange={ this.handlePasswordChange.bind(this) }/>
                        <div className="loginButton" onClick={()=>this.authenticateUser()}>Submit</div>
                    </div>
                </div>
            )
        }
    }

    render() {
        return (
            <div className="profileContainer">
                {this.getProfileComponent()}
            </div>
        );
    }
}

export default Profile;