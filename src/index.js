
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App/App';
import Profile from './Profile/Profile';
import configureStore from './store';
import { Provider } from 'react-redux';

import {
  BrowserRouter as Router,
  Redirect,
  Route
} from 'react-router-dom';

import './index.css';

const store = configureStore();


ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Route path="/home" component={App}></Route>
        <Route path="/profile" component={Profile} />
      </div>
    </Router>
  </Provider>,
  document.getElementById('root')
);