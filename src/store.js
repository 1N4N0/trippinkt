import { 
  createStore, 
  applyMiddleware, 
  compose
} from 'redux';
import createSagaMiddleware from 'redux-saga';

// root reducer
import rootReducer from './rootReducer';

import rootSaga from './rootSaga';

// redux devtools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware(); 
// create the store

const configureStore = () => {
  const store = {
    ...createStore(rootReducer, composeEnhancers(
      applyMiddleware(sagaMiddleware)))
  };

   sagaMiddleware.run(rootSaga);
    
  return store;
};


export default configureStore;